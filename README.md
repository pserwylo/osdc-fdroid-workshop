# [Submitting your Android app to F-Droid](https://2015.osdc.com.au/schedule/presentation/11/)

## Goals

This workshop is for people interested in Android development, or those interested in free software and getting it to a wider audience.

By the end of this workshop, you should be able to:

 * Submit open source apps to f-droid.org for distribution, and
 * Create your own repository of apps, that people can access via the F-Droid Android app.

## Overview

This workshop will have three main parts:

 * [Familiarising yourself with the tools](setting-up.md)
 * [Publishing prebuilt binary .apks](binary-repo.md)
 * [Building .apks from source then publishing](build-from-source.md)

If there is time, we will also touch on the topic of using a [build server](build-server.md).

## Getting Help

During the workshop, I'll will be walking through the instructions here, and helping out where possible.
After the workshop, there are plenty of other sources of information to help.

The most important document when working with F-Droid is perhaps the [Server Manual](https://f-droid.org/manual/fdroid.html). This is the authoritative reference for the `fdroid` tool and associated tools. It is also the reference for the F-Droid build recipe files.

Much of the information here is paraphrasing content from the [F-Droid wiki](https://f-droid.org/wiki), but tailored for this workshop and the specific goals of it. Pages you may find interesting include:

 * [Setup an F-Droid App Repo](https://f-droid.org/wiki/page/Setup_an_FDroid_App_Repo)