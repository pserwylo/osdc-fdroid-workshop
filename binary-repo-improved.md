# Adding Metadata to your Binary Repository

In the [previous section](binary-repo.md), we created a bare bones F-Droid repository with a single app in it. This will build on that by adding some metadata about the repository and the app.

<div>
	<img alt="App without metadata" src="images/fdroid-sans-metadata.png" style="display: inline; width: 200px" />
	<img alt="App with metadata" src="images/fdroid-with-metadata.png" style="display: inline; width: 200px" />
</div>

## Goals

By the end of this:

 * You should be aware of the `fdroid lint` command
 * The app in your repo should have appropriate metadata to be used by the F-Droid client
 * Your binary repo should have a name + description

## Howto

### Check the Metadata of your Repository

From the directory of your binary repository, run the `fdroid lint` command.
With some luck, you should see the following output:

```
org.fdroid.fdroid: License 'Unknown': No license specified
org.fdroid.fdroid: Category 'None' is not valid
org.fdroid.fdroid: Description 'F-Droid' is just the app's summary
```

You may have guessed from that output, or your previous experience with a linter, that `fdroid lint` will check the metadata of your repository and notify you of anything that is undesirable or problematic.

### Update the Metadata

In the last workshop when we ran `fdroid update --create-metadata`, fdroid created a file called `metadata/org.fdroid.fdroid.txt`.
This file is what fdroiid uses to construct the repository. 

The contents of the file should be:

```
License:Unknown
Web Site:
Source Code:
Issue Tracker:
Changelog:
Summary:F-Droid
Description:
F-Droid
.
```

To fully understand all of these fields, the best place to look is the [F-Droid server manual](https://f-droid.org/manual/fdroid.html).

Lets fill in a few of these fields:

```
License:GPLv3+
Web Site:https://f-droid.org
Source Code:https://gitlab.com/fdroid/fdroidclient
Issue Tracker:https://gitlab.com/fdroid/fdroidclient/issues
Changelog:https://gitlab.com/fdroid/fdroidclient/tree/CHANGELOG
Summary:Application manager
Description:
F-Droid allows you to download apps from different sources, install them on your device, be notified of updates, etc.
.
```

After doing so, we can run `fdroid lint` again from the repo directory:

```
org.fdroid.fdroid: Category 'None' is not valid
```

This is because I forgot to add the Category tag when modifying the metadata.
Adding the following line to the file does the trick:

```
Categories:System
```

### Cleaning up/Normalizing the Metadata

Once you are happy with the metadata in `metadata/org.fdroid.fdroid.txt` then you can run the `fdroid rewritemeta` command to normalize the file.
This will ensure each metadata file has the same format (e.g. the order of each field in the file).

### Rebuild your Index

Making changes ot the metadata means that the repository needs to be updated with `fdroid update`.
Note that `--create-metadata` is not required any more, that was for the first step when there was not any metadata present.

After doing this:

* Copy the repo to your webroot (`fdroid server --local-copy-dir /tmp/http-server/fdroid update`)
* (Make sure web server is actually running)
* Open F-Droid on your device
* Refresh the list of apps from the main screen

This should result in the newly added metadata being visible in F-Droid.

## Next Step

Binary repositories can suffice well for your own repositories. However to submit apps to the main F-Droid.org repository, you'll need to [build from source](build-from-source.md).