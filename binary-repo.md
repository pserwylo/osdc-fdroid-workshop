# Getting a Feel for fdroidserver: Creating a Binary Repository

Before we work towards submitting an app to the main F-Droid repository, this task is designed to familiarise ourselves with the fdroidserver tools.

## Goals

By the end of this, you should be able to:

 * Configure the F-Droid app with your own repository.
 * Download your application from that repo from within the F-Droid app.
 * Get notifications of updates, etc.
 * Optionally make it publicly accessible for other F-Droid users.

This is done by creating your own F-Droid repository which hosts prebuilt Android apps. By hosting this repository online, anyone with the F-Droid Android app and a working internet connection can use this repo.

This is useful for two reasons:

 * Getting familiar with the fdroidserver and required metadata (for the rest of this workshop).
 * Self hosting apks so they can be downloaded by the F-Droid client (for real life distribution scenarios).

## Howto

### Initialize Empty Repository

Enter an empty directory and run `fdroid init` ([view example output](assets/binary-repo/fdroid-init.log)).

This should result in the following directory structure:

```
pete@epsilon:/tmp/test-repo$ ls -lah
total 28K
drwxr-xr-x  3 pete pete  160 Oct 24 08:32 .
drwxrwxrwt 11 root root  360 Oct 24 08:31 ..
-rw-------  1 pete pete 9.1K Oct 24 08:31 config.py
-rw-r--r--  1 pete pete 3.3K Oct 24 08:31 fdroid-icon.png
-rw-------  1 pete pete   44 Oct 24 08:31 .fdroid.keypass.txt
-rw-------  1 pete pete   44 Oct 24 08:31 .fdroid.keystorepass.txt
-rw-------  1 pete pete 3.8K Oct 24 08:31 keystore.jks
drwxr-xr-x  2 pete pete   40 Oct 24 08:31 repo
```

### Add an .apk

For this workshop, lets add the F-Droid .apk to our repository.

From your repository directory (the one with config.py) run `fdroid update --create-metadata` ([view example output](assets/binary-repo/fdroid-update-create-metadata.log)).

#### Metadata

The main thing this did was to create the `metadata/org.fdroid.fdroid.txt` file.
File in the `metadata/` directory tell F-Droid data what to include in a repository, how to build it (not required for a binary repo), license, categories, changelog, donation information, etc:

```
pete@epsilon:/tmp/test-repo$ ls -lah metadata/
total 4.0K
drwxr-xr-x 2 pete pete  60 Oct 24 08:34 .
drwxr-xr-x 7 pete pete 240 Oct 24 08:34 ..
-rw-r--r-- 1 pete pete 104 Oct 24 08:34 org.fdroid.fdroid.txt
```

We will revisit this later in this section of the workshop, and in much greater detail later on when building .apks from source.

#### Index

The other thing the `fdroid update` command did was to create an `index.xml`, and a signed copy of that in `index.jar` in the `repo/` subdirectory.
It also extracted the relevant icons for different screen sizes from the .apk and put them in the `icon-*` directories.

```
pete@epsilon:/tmp/test-repo$ ls -lah repo/
total 3.5M
drwxr-xr-x 9 pete pete  260 Oct 24 08:34 .
drwxr-xr-x 7 pete pete  240 Oct 24 08:34 ..
-rw-r--r-- 1 pete pete    0 Oct 24 08:34 categories.txt
-rw-r--r-- 1 pete pete 3.5M Oct 24 08:34 FDroid-stable.apk
drwxr-xr-x 2 pete pete   80 Oct 24 08:34 icons
drwxr-xr-x 2 pete pete   60 Oct 24 08:34 icons-120
drwxr-xr-x 2 pete pete   60 Oct 24 08:34 icons-160
drwxr-xr-x 2 pete pete   60 Oct 24 08:34 icons-240
drwxr-xr-x 2 pete pete   60 Oct 24 08:34 icons-320
drwxr-xr-x 2 pete pete   60 Oct 24 08:34 icons-480
drwxr-xr-x 2 pete pete   60 Oct 24 08:34 icons-640
-rw-r--r-- 1 pete pete 5.1K Oct 24 08:34 index.jar
-rw-r--r-- 1 pete pete 4.0K Oct 24 08:34 index.xml
```

## Publish your repo

**DO NOT expose the directory with `config.py`, `.fdroid.keypass.txt`, and `.fdroid.keystorepass.txt` to your webserver!**
**This will allow people to get access to your signing keys.**

The `repo/` subdirectory is what constitutes the "F-Droid repository".
It contains the metadata required by the client (`index.jar`), relevant icons for all apps provided by that index (`icons-*`) and the .apk files (`FDroid-stable.apk` in this case).

To make this available, copy it to the webroot of your webserver:

```
fdroid server --local-copy-dir /tmp/http-server/fdroid update
```

Note that the `fdroid server update` command expects the repo to live in an `fdroid/` subdirectory in your webroot.
This is not required, but helps because the client will automatically look for `fdroid/repo/index.jar` if you only type the path to the root of the web server.

In this workshop, we'll use a temporary webserver:

```
cd /tmp/http-server/

python -m SimpleHTTPServer 8000 # using for python2, or
python -m http.server 8000 # using python3, or
php -S 0.0.0.0:8000 # using PHP
```

## Test the repo in the F-Droid client

Follow these steps to configure F-Droid with your newly created repository. 

### 1) Open F-Droid and select "Repositories" from the menu

<div>
	<img alt='"Repositories" menu item' src="images/manage-repos-menu-item.png" style="display: inline;" />
	<img alt='"Repositories" view' src="images/manage-repositories-list.png" style="display: inline;" />
</div>

### 2) For testing, disable all repositories

This will make it easier to identify the apps specified by our repository.

### 3) Add a new repository with the "+" button

Don't forget to add the port of your HTTP server.
Ignore the "Fingerprint" for now.

![Add new repository](images/add-repo.png)

### 4) Wait for update to complete

This should take less than a second on a local network, with only a single app in our repository.

![New repository](images/newly-added-repo.png)

### 5) Return to the main screen to view apps

Ensure the "All" category is selected from the main drop down if you can't see anything.

![List of apps](images/list-of-apps.png)

## Next Step

This repo is a good start, but it doesn't descriptions, license infrormation, or other metadata. The [next section](binary-repo-improved.md) will improve on this binary repo.