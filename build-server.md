# Extra stuff for if there is time

## Build server

If you are hosting a repository which builds .apks you don't trust, then build them in an isolated environment. fdroidserver makes this relatively straightforward (if not slow) by interfacing with [Vagrant]() to execute all builds in a virtual machine.