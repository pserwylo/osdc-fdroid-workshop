# Setting Up

## Supported Operating Systems

As of now, the fdroidserver tools support Linux and Mac. In the future they may support Windows and [merge requests](https://gitlab.com/fdroid/fdroidserver) are welcome!

## Dependencies

### [fdroidserver](https://gitlab.com/fdroid/fdroidserver)

For this workshop, you can either:

#### Install from USB/DVD in workshop

The fdroidserver.tar.gz file on the USB/DVD has a Python 2.7 virtualenv in the env/ directory.
It should have the dependencies it needs already there, ready to run:

```
tar -xvzf fdroidserver.tar.gz
cp -r fdroidserver /somewhere/on/your/machine/
cd /somewhere/on/your/machine/fdroidserver
source env/bin/activate
python2 setup.py install
```

#### Download from a package manager

F-Droid is available from Apt, Pacman, MacPorts, pip, and easy\_install.
Some instructions are available on the [fdroidserver README](https://gitlab.com/fdroid/fdroidserver#installing).

### [Android SDK](https://developer.android.com/sdk/index.html#Other)
The Android SDK is several GiB and likely too large for conference WiFi to handle nicely.

If you already have it installed, then that is fine.

Otherwise, the options are likely:

#### Install SDK from USB/DVD in workshop

The file android-sdk.tar.gz should have most Android platforms provided.
Copy it to your machine (so other people can use the USB/DVD if desired) and then untar it to a directory of your choice.

#### Import .ova VirtualBox VM from USB

I'm not 100% sure this will work, but I've provided a USB with a 7.4GiB VirtualBox VM exported to the .ova format.
The VM is the result of running `./makebuildserver` in the fdroidserver directory.

To import it into VirtualBox:
 * Copy the file from USB to your machine (so other people can use the USB if desired)
 * Open VirtualBox
 * File -> Import Appliance and follow the prompts

The username/password for this machine is vagrant/vagrant.
You will have to setup port forwarding in VirtualBox so that a port (such as port 2222) on 127.0.0.1 of the host machine will be redirected to port 22 of the guest machine (usually 10.0.2.15).
You may also have to enable the "PAE" setting by selecting the machine -> Settings -> System -> Enable PAE/...

### [Gradle](http://gradle.org/)

Be aware that even though there is physical media available in the workshop that has Gradle on it, the first time you run Gradle on an Android project it will likely try to download a small subset of the internet.

#### Install from USB/DVD in workshop

#### Install from package manager

#### Install from upstream

## Downloads

A tool like fdroidserver exists to build aarbitrary different apps neccesarily needs a lot of build tools to be installed.
Even publishing existing binary apps requires the Android SDK tools to parse the .apk files for metadata.

This workshop will have USBs, DVDs, and a LAN HTTP server with the following required software:

And the following optional software
 * [fdroiddata](https://gitlab.com/fdroid/fdroiddata)
 * [Android NDK](https://developer.android.com/ndk/downloads/index.html)
 * [Ant](https://ant.apache.org/)

If you already have these installed, then there is likely no need to download/install them any more.
Your package manager of choice will likely have packages for `gradle` and `ant` if you prefer.

## Next step

Now that we have set up the F-Droid server tools installed, lets [setup a repository containing prebuild binaries](binary-repo.md).